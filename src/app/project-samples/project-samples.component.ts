import { Component } from '@angular/core';
import { ImageModalComponent } from './image-modal/image-modal.component'; 
import { DialogService } from "ng2-bootstrap-modal";

@Component({
  selector: 'app-project-samples',
  templateUrl: './project-samples.component.html',
  styleUrls: ['./project-samples.component.css']
})
export class ProjectSamplesComponent {

  constructor(private dialogService:DialogService) { }

  showConfirm(url, title, description) {
    let disposable = this.dialogService.addDialog(ImageModalComponent, {
      imageURL: url,
      imageTitle: title,
      imageDescription: description
    })
        .subscribe((isConfirmed)=>{
            //We get dialog result
        });
    //We can close dialog calling disposable.unsubscribe();
    //If dialog was not closed manually close it by timeout
}
}
