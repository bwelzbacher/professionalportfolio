import { Component, OnInit } from '@angular/core';
import { ITile } from './models/tile.model';
import { IBoard } from './models/board.model';
import { NewGamesModalComponent } from './new-games-modal/new-games-modal.component';
import { DialogService } from "ng2-bootstrap-modal";

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent implements OnInit{
  tile: ITile;
  board: IBoard;
  tileValues = ["Peanut Butter", "Donut", "Taco", "Pizza", "Ice Cream", "Tiramisu", "Hamburger", "Hotdog", "French Fries", "Chicken Nugget"];
  selectedTiles: ITile[];
  time: number;
  matchPoints: number;
  timePoints: number;
  clickPoints: number;
  isGameOver: boolean;
  isPaused: boolean;
  finalScore: number;
  disposable: any;

  constructor(private dialogService:DialogService) {
   this.board = this.setupBoard();
   this.selectedTiles = [];
   this.time = 60;
   this.matchPoints = 0;
   this.timePoints = 0;
   this.clickPoints = 0;
   this.finalScore = 0;
   this.isGameOver = false;
   this.isPaused = false;
  }

 ngOnInit() {
 setTimeout(()=>{ this.showConfirm(); },1);
 }

 ngOnDestroy() {
  this.time = 0;
  // this.disposable.unsubscribe();
  // this.disposable.destination.unsubscribe();

}

//New Game & Score Modal
  showConfirm() {
      this.isPaused = true;
      this.disposable = this.dialogService.addDialog(NewGamesModalComponent, {
          click: this.clickPoints,
          matches: this.matchPoints,
          time: this.timePoints,
          total: this.clickPoints + this.matchPoints + this.timePoints
  })
          .subscribe((isConfirmed)=>{
              //We get dialog result
              if(isConfirmed) {
                  this.refreshGame();
              }
          });
  }

//Generate and Shuffle Tiles
  setupBoard() {
    let counter = 0;
    let duplicateCounter=0;
    let tileList = [];
    while(counter < 20){
      let valueCounter = counter - duplicateCounter;
      tileList.push({value: this.tileValues[valueCounter], id: counter + 1, isMatched: false})
        if (counter%2 == 0) {
          duplicateCounter++;
        }
      counter++;
    }
    return {tiles: this.shuffle(tileList)};
  }

  shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
  }

//Subtract Second Intervals From Time
  tickTimer() {
      let self = this;
      if(self.time > 0 && !this.isGameOver && !this.isPaused){
        setTimeout(function(){
          self.time--;
          self.tickTimer();
          },1000);
      }
      if(self.time == 0) {
        this.endGame();
      }
  }

//See If Clicked Tile Is Selectable and Select
  selectTiles(tile) {
      if (tile.isMatched) {
      return;
    }

    if (tile == this.selectedTiles[0]){
      this.selectedTiles.length = 0;
      return;
    }

      if (this.selectedTiles.length >= 2) {
      return;
    }
    this.selectedTiles.push(tile);
  }

//Score clicks and match points
  scoreSelected(tile) {
    this.selectTiles(tile);
    if (this.selectedTiles.length == 2) {
      this.clickScore();
      if (this.selectedTiles[0].value == this.selectedTiles[1].value) {
        this.matchScore();
        console.log("Is Matched");
        this.selectedTiles[0].isMatched = true;
        this.selectedTiles[1].isMatched = true;
        this.checkWin();
      }
      setTimeout(()=>{ this.selectedTiles.length = 0; },500);
    }
  }

//Check if Already Selected
 isSelected(tile) {
  let alreadySelected = false;
  this.selectedTiles.forEach(function(selectedTile){
    if(selectedTile == tile){
      alreadySelected = true;
    }
  })
 return alreadySelected;
 }


//Get Scores
  clickScore(){
    this.clickPoints -= 2;
  }

  matchScore(){
   this.matchPoints += 10;
  }

//End Game
 endGame() {
  this.isGameOver = true;
  this.timePoints = this.time;
  this.showConfirm();
   }

//Check if all matches were found, If YES, End Game
 checkWin() {
 let allMatched = true;
  this.board.tiles.forEach(function(tile){
    if(!tile.isMatched){
      allMatched = false;
    }
  })
 if (allMatched) {
    this.endGame();
  }
 }

//Default settings for New Game
 refreshGame() {
   this.isPaused = false;
   this.board = this.setupBoard();
   this.selectedTiles = [];
   this.time = 60;
   this.matchPoints = 0;
   this.timePoints = 0;
   this.clickPoints = 0;
   this.finalScore = 0;
   this.isGameOver = false;
   this.tickTimer();
 }

}
