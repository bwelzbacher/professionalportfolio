import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewGamesModalComponent } from './new-games-modal.component';

describe('NewGamesModalComponent', () => {
  let component: NewGamesModalComponent;
  let fixture: ComponentFixture<NewGamesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewGamesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewGamesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
