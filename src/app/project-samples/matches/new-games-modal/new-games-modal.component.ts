import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
export interface ConfirmModel {
  // click:number;
  // matched:number;
  // time:number;
  // total:number;
}

@Component({
  selector: 'app-new-games-modal',
  templateUrl: './new-games-modal.component.html',
  styleUrls: ['./new-games-modal.component.css']
})
export class NewGamesModalComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel {
  constructor(dialogService: DialogService) {
    super(dialogService);
  }
  confirm() {
    // we set dialog result as true on click on confirm button,
    // then we can get dialog result from caller code
    this.result = true;
    this.close();
  }
  ngOnInit() {
  }

}