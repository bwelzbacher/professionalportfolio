export interface ITile {
    value: string;
    id: number;
    isMatched: boolean;
  }
  