export interface IDice {
    value: number;
    isHeld: boolean;
  }
  