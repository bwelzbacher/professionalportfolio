import { IDice } from './dice.model';

export interface IBoard {
  dice: IDice[];

    ones: {
      isLockedIn: boolean;
      finalPoints: number;
    }

    twos: {
      isLockedIn: boolean;
      finalPoints: number;
    }

    threes: {
      isLockedIn: boolean;
      finalPoints: number;
    }

    fours: {
      isLockedIn: boolean;
      finalPoints: number;
    }

    fives: {
      isLockedIn: boolean;
      finalPoints: number;
    }

    sixes: {
      isLockedIn: boolean;
      finalPoints: number;
    }

    three_kind: {
      isLockedIn: boolean;
      finalPoints: number;
    }

    four_kind: {
      isLockedIn: boolean;
      finalPoints: number;
    }

    sm_straight: {
      isLockedIn: boolean;
      finalPoints: number;
    }

    lg_straight: {
      isLockedIn: boolean;
      finalPoints: number;
    }

    full_house: {
      isLockedIn: boolean;
      finalPoints: number;
    }

    chance: {
      isLockedIn: boolean;
      finalPoints: number;
    }

    five_kind: {
      isLockedIn: boolean;
      finalPoints: number;
    }
}
