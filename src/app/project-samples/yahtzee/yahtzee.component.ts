import { Component } from '@angular/core';
import { IDice } from './models/dice.model';
import { IBoard } from './models/board.model';

@Component({
  selector: 'app-yahtzee',
  templateUrl: './yahtzee.component.html',
  styleUrls: ['./yahtzee.component.css']
})
export class YahtzeeComponent {
  die: IDice;
  board: IBoard;
  orderedDice: IDice[];
  rollCount: number;
  totalScore: number;
  bonus: number;
  pointsNeeded: number;
  bonusAchieved: boolean;
  bonusYahtzeeAvailable: boolean;
  bonusYahtzeeEarned: boolean;
  scoreSelected: boolean;
  canHold: boolean;

  constructor() {
    this.board = {
      dice: this.generateDice(),
      ones: {
        isLockedIn: false,
        finalPoints: 0
      },
      twos: {
        isLockedIn: false,
        finalPoints: 0
      },
      threes: {
        isLockedIn: false,
        finalPoints: 0
      },
      fours: {
        isLockedIn: false,
        finalPoints: 0
      },
      fives: {
        isLockedIn: false,
        finalPoints: 0
      },
      sixes: {
        isLockedIn: false,
        finalPoints: 0
      },
      three_kind: {
        isLockedIn: false,
        finalPoints: 0
      },
      four_kind: {
        isLockedIn: false,
        finalPoints: 0
      },
      sm_straight: {
        isLockedIn: false,
        finalPoints: 0
      },
      lg_straight: {
        isLockedIn: false,
        finalPoints: 0
      },
      full_house: {
        isLockedIn: false,
        finalPoints: 0
      },
      chance: {
        isLockedIn: false,
        finalPoints: 0
      },
      five_kind: {
        isLockedIn: false,
        finalPoints: 0
      }
    }
    this.orderedDice = [];
    this.rollCount = 0;
    this.totalScore = 0;
    this.bonus = 0;
    this.pointsNeeded = 63;
    this.bonusAchieved = false;
    this.bonusYahtzeeAvailable = false;
    this.bonusYahtzeeEarned = false;
    this.scoreSelected = false;
    this.canHold = false;

  }

  generateDice() {
    let counter = 0;
    let diceList: IDice[] = [];
    while (counter < 5) {
      diceList.push(<IDice>{value: 1, isHeld: false});
      counter++;
    }
    return diceList;
  }

  holdDice(dice) {
    if (dice.isHeld == false) {
      dice.isHeld = true;
    }
    else {
      dice.isHeld = false;
    }
  }

  calculatePoints(category, lockedPoints) {
    if(this.scoreSelected == true) {
      alert("You already scored this round. Please roll to continue.");
    }
    else {
    category.isLockedIn = true;
    category.finalPoints = lockedPoints;
    }
  }

  resetNewRound() {
    this.rollCount = 0;
    this.canHold = false;
    if(this.scoreSelected == false) {
      this.scoreSelected = true;
      for (var i=0; i < 5; i++) {
        this.board.dice[i].isHeld = false;
      }

      let upperScore = this.board.ones.finalPoints + this.board.twos.finalPoints + this.board.threes.finalPoints +
        this.board.fours.finalPoints + this.board.fives.finalPoints + this.board.sixes.finalPoints;

        this.pointsNeeded = 63 - upperScore;

        if (upperScore >= 63) {
          this.bonusAchieved = true;
          this.bonus = 35;
        }

      this.totalScore =  upperScore + this.board.three_kind.finalPoints + this.board.four_kind.finalPoints + this.board.full_house.finalPoints + this.board.sm_straight.finalPoints
        + this.board.five_kind.finalPoints + this.board.chance.finalPoints + this.board.lg_straight.finalPoints + this.bonus;
    }
}

  rollDice() {
     this.rollCount++;
     this.scoreSelected = false;
     this.canHold = true;
      if (this.rollCount >= 4) {
        alert ("You have already rolled 3 times. Please select your score to continue.");
      }
      else {
         for (var i=0; i < 5; i++) {
            if(this.board.dice[i].isHeld == false) {
            this.board.dice[i].value = Math.floor(Math.random() * Math.floor(6))+1;
            }
          }
      }
    this.orderedDice = this.board.dice.map(x => Object.assign({}, x));
    this.sortDice();
  }

  sortDice() {
      this.orderedDice.sort((a: IDice, b: IDice) => {
      if (a.value <= b.value){
        return 1;
      }
      else{
        return -1;
      }
    })
  }

  countNumbers(num) {
    let total = 0;
    for (var i=0; i < 5; i++) {
      if (this.orderedDice[i].value == num) {
        total += num;
      }
    }
    return total;
  }

  threeKind() {
    let total = 0;
    let diceTotal = 0;
    let pairs = 0;

   for (var i=0; i < 5; i++) {
     diceTotal += this.orderedDice[i].value
   }

    for (var i=0; i < 4; i++) {
      if (this.orderedDice[i].value == this.orderedDice[i+1].value) {
        pairs++;
        if (pairs == 2) {
          total = diceTotal;
          break;
        }
        else total = 0;
      }
      else pairs = 0;
    }
    return total;
  }

  fourKind() {
    let total = 0;
    let diceTotal = 0;
    let pairs = 0;

    for (var i=0; i < 5; i++) {
      diceTotal += this.orderedDice[i].value;
    }

    for (var i=0; i < 4; i++) {
      if (this.orderedDice[i].value == this.orderedDice[i+1].value) {
        pairs++;
        if (pairs == 3) {
          total = diceTotal;
          break;
        }
        else total = 0;
      }
      else { pairs = 0; }
    }

    return total;
  }

   fullHouse() {
    let total = 0;
    let sameNum = 0;
    let difNums = 0;

    for (var i=0; i < 4; i++) {
      if (this.orderedDice[i].value == this.orderedDice[i+1].value) {
        sameNum++;
      }
      else {
       difNums++;
       sameNum = 0;
      }

      if ((sameNum == 1 || sameNum == 2) && difNums == 1) {
        total = 25;
      }
      else total = 0;
    }
    return total;
  }

  smStraight() {
    let total = 0;
    let count = 0;

    for (var i=0; i < 4; i++) {
      if (this.orderedDice[0].value == (this.orderedDice[4].value + 3)) {
        if (this.orderedDice[i].value == (this.orderedDice[i+1].value + 1)) {
        count++;
          if (count == 3) {
            total = 30;
            break;
          }
        }
      }

      else if (this.orderedDice[i].value == (this.orderedDice[i+1].value + 1)) {
        count++;
        if (count == 3) {
          total = 30;
          break;
        }
      }
      else count = 0;
    }
    return total;
  }

  lgStraight() {
    let total = 0;
    let pairs = 0;

    for (var i=0; i < 4; i++) {
      if (this.orderedDice[i].value != (this.orderedDice[i+1].value + 1)) {
        pairs++;
      }
    }
    if (pairs == 0) {
      total = 40;
    }
    else {
    total = 0;
    }
    return total;
  }

  chance() {
    let total = 0;
    for (var i=0; i < 5; i++) {
      total += this.orderedDice[i].value;
    }
    return total;
  }

  yahtzee() {
    let pairs = 0;
    let total = 0;

    for (var i=0; i < 4; i++) {
      if (this.orderedDice[i].value == this.orderedDice[i+1].value) {
        pairs++;
      }
    }

    if (pairs == 4) {
      total = 50;
    }
    else total = 0;

    return total;
  }

}

