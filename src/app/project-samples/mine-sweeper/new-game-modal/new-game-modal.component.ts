import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";

export interface ConfirmModel {
  // bombsFound:number;
  // time:number;
  // option:number;
}

@Component({
  selector: 'app-new-game-modal',
  templateUrl: './new-game-modal.component.html',
  styleUrls: ['./new-game-modal.component.css']
})
export class NewGameModalComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel {
  constructor(dialogService: DialogService) {
    super(dialogService);
  }
  confirm() {
    this.result = true;
    this.close();
  }
  ngOnInit() {
  }

}
