import { Component } from '@angular/core';
import { ITile } from './models/tile.model';
import { IBoard } from './models/board.model';
import { NewGameModalComponent } from './new-game-modal/new-game-modal.component';
import { DialogService } from "ng2-bootstrap-modal";

@Component({
  selector: 'app-mine-sweeper',
  templateUrl: './mine-sweeper.component.html',
  styleUrls: ['./mine-sweeper.component.css']
})
export class MineSweeperComponent {
  tile: ITile;
  board: IBoard;
  bombs: number;
  isDefusing: boolean;
  countBombs: number;
  bombsFound: number;
  bombsLeft: any;
  time: number;
  isPaused: boolean;
  option: number;
  find: any;

  constructor(private dialogService:DialogService) {
    this.bombs = 25;
    this.board = this.setupBoard();
    this.assignBombs();
    this.generateNumbers();
    this.removeZeros();
    this.isDefusing = false;
    this.bombsLeft = this.bombs;
    this.bombsFound = 0;
    this.time = 0;
    this.isPaused = false;
    this.option = 0;
  }

   ngOnInit() {
 setTimeout(()=>{ this.showConfirm(); },1);
 }

setupBoard() {
  let counter = 0;
  let tileList = []
  while(counter < 144){
    tileList.push({isFound: false, value: 0});
    counter++;
  }
    return {tiles: tileList};
}

handleClick(tile, index) {
  if (!this.isDefusing) {
      tile.isFound = true;
      this.checkIfBlank(tile, index);
    if (tile.value == "B") {
      this.detonateBomb(tile);
    }
  }
  else {this.defuseMode(tile);}
}

tickTimer() {
    let self = this;
    if(!this.isPaused){
      setTimeout(function(){
        self.time++;
        self.tickTimer();
        },1000);
    }
}

showConfirm() {
    this.isPaused = true;
    let disposable = this.dialogService.addDialog(NewGameModalComponent, {
        bombsFound: this.bombs - this.bombsLeft,
        time: this.time,
        option: this.option
})
        .subscribe((isConfirmed)=>{
            //We get dialog result
            if(isConfirmed) {
                this.newGame();
            }
        });
}

assignBombs() {
  for (var i=0; i < this.bombs; i++) {
    var bombPosition = Math.floor(Math.random() * Math.floor(144));
    if (this.board.tiles[bombPosition].value == "B") {
      i--;
    }
    else {this.board.tiles[bombPosition].value = "B";}
  }
}

showBombs() {
  this.board.tiles.forEach(function(tile){
    if (tile.value == "B") {
      tile.isFound = true;
      tile.isExploded = true;
    }
  })
}

detonateBomb(tile) {
  tile.isExploded = true;
  this.showBombs();
  this.option = 1;
  this.showConfirm();
}

defuseMode(tile) {
  if (tile.isFound == true) {
    tile.isDefused = false;
  }
  else {
    if (tile.value != "B") {
       tile.isDefused = true;
       this.option = 2;
       this.showConfirm();
    }
    else {
      tile.isDefused = true;
      this.bombsLeft --;
      if (this.bombsLeft == 0) {
        this.option = 3;
        this.showConfirm();
      }
    }
  }
}

generateNumbers() {
  let self = this;
  this.board.tiles.forEach(function(tile, index){
    if(tile.value == "B"){
      switch (true) {
        case (index > 0 && index < 11):
          if (Number.isInteger(self.board.tiles[index - 1].value)) { self.board.tiles[index - 1].value += 1};
          if (Number.isInteger(self.board.tiles[index + 11].value)) { self.board.tiles[index + 11].value += 1};
        case (index == 0):
          if (Number.isInteger(self.board.tiles[index + 13].value)) { self.board.tiles[index + 13].value += 1};
          if (Number.isInteger(self.board.tiles[index + 12].value)) { self.board.tiles[index + 12].value += 1};
          if (Number.isInteger(self.board.tiles[index + 1].value)) { self.board.tiles[index + 1].value += 1};
          break;

        case ((index+1)%12 === 0 && index != 11 && index != 143):
          if (Number.isInteger(self.board.tiles[index - 12].value)) { self.board.tiles[index - 12].value += 1};
          if (Number.isInteger(self.board.tiles[index - 13].value)) { self.board.tiles[index - 13].value += 1};
        case (index == 11):
          if (Number.isInteger(self.board.tiles[index + 12].value)) { self.board.tiles[index + 12].value += 1};
          if (Number.isInteger(self.board.tiles[index + 11].value)) { self.board.tiles[index + 11].value += 1};
          if (Number.isInteger(self.board.tiles[index - 1].value)) { self.board.tiles[index - 1].value += 1};
          break;

        case (index%12 === 0 && index != 132):
          if (Number.isInteger(self.board.tiles[index + 13].value)) { self.board.tiles[index + 13].value += 1};
          if (Number.isInteger(self.board.tiles[index + 12].value)) { self.board.tiles[index + 12].value += 1};
        case (index == 132):
          if (Number.isInteger(self.board.tiles[index + 1].value)) { self.board.tiles[index + 1].value += 1};
          if (Number.isInteger(self.board.tiles[index - 11].value)) { self.board.tiles[index - 11].value += 1};
          if (Number.isInteger(self.board.tiles[index - 12].value)) { self.board.tiles[index - 12].value += 1};
          break;

        default:
          if (Number.isInteger(self.board.tiles[index + 13].value)) { self.board.tiles[index + 13].value += 1};
          if (Number.isInteger(self.board.tiles[index + 12].value)) { self.board.tiles[index + 12].value += 1};
          if (Number.isInteger(self.board.tiles[index + 11].value)) { self.board.tiles[index + 11].value += 1};
        case (index > 132 && index < 143):
          if (Number.isInteger(self.board.tiles[index + 1].value)) { self.board.tiles[index + 1].value += 1};
          if (Number.isInteger(self.board.tiles[index - 11].value)) { self.board.tiles[index - 11].value += 1};
        case (index == 143):
          if (Number.isInteger(self.board.tiles[index - 1].value)) { self.board.tiles[index - 1].value += 1};
          if (Number.isInteger(self.board.tiles[index - 12].value)) { self.board.tiles[index - 12].value += 1};
          if (Number.isInteger(self.board.tiles[index - 13].value)) { self.board.tiles[index - 13].value += 1};
          break;
      }
    }
  })
}

  removeZeros() {
    this.board.tiles.forEach(function(tile){
      if (tile.value == 0) {
        tile.value = "";
      }
    })
  }


  checkIfBlank (tile, i) {
  console.log("CIB", tile, i);
    if (tile.value == "") {
      tile.isFound = true;
      this.findNeighbors(tile, i);
      console.log("checkIfBlank", tile, i);
    }
    else if (typeof tile.value == "number") {
      tile.isFound = true;
    }
  }

  findNeighbors(tile,index) {
    switch (true) {
      case (index > 0 && index < 11):
          if (!this.board.tiles[index + 11].isFound){this.checkIfBlank(this.board.tiles[index + 11], (index + 11));}
          if (!this.board.tiles[index - 1].isFound){this.checkIfBlank(this.board.tiles[index - 1], (index - 1));}
      case (index == 0):
          if (!this.board.tiles[index + 13].isFound){this.checkIfBlank(this.board.tiles[index + 13], (index + 13));}
          if (!this.board.tiles[index + 12].isFound){this.checkIfBlank(this.board.tiles[index + 12], (index + 12));}
          if (!this.board.tiles[index + 1].isFound){this.checkIfBlank(this.board.tiles[index + 1], (index + 1));}
          break;

      case ((index+1)%12 === 0 && index != 11 && index != 143):
          if (!this.board.tiles[index - 13].isFound){this.checkIfBlank(this.board.tiles[index - 13], (index - 13));}
          if (!this.board.tiles[index - 12].isFound){this.checkIfBlank(this.board.tiles[index - 12], (index - 12));}
      case (index == 11):
          if (!this.board.tiles[index - 1].isFound){this.checkIfBlank(this.board.tiles[index - 1], (index - 1));}
          if (!this.board.tiles[index + 11].isFound){this.checkIfBlank(this.board.tiles[index + 11], (index + 11));}
          if (!this.board.tiles[index + 12].isFound){this.checkIfBlank(this.board.tiles[index + 12], (index + 12));}
          break;

      case (index%12 === 0 && index != 132):
          if (!this.board.tiles[index + 13].isFound){this.checkIfBlank(this.board.tiles[index + 13], (index + 13));}
          if (!this.board.tiles[index + 12].isFound){this.checkIfBlank(this.board.tiles[index + 12], (index + 12));}
      case (index == 132):
          if (!this.board.tiles[index - 12].isFound){this.checkIfBlank(this.board.tiles[index - 12], (index - 12));}
            console.log(this.board.tiles[index - 12]);
          if (!this.board.tiles[index - 11].isFound){this.checkIfBlank(this.board.tiles[index - 11], (index - 11));}
            console.log(this.board.tiles[index - 11]);
          if (!this.board.tiles[index + 1].isFound){this.checkIfBlank(this.board.tiles[index + 1], (index + 1));}
          break;

      default:
          if (!this.board.tiles[index + 13].isFound){this.checkIfBlank(this.board.tiles[index + 13], (index + 13));}
          if (!this.board.tiles[index + 12].isFound){this.checkIfBlank(this.board.tiles[index + 12], (index + 12));}
          if (!this.board.tiles[index + 11].isFound){this.checkIfBlank(this.board.tiles[index + 11], (index + 11));}
      case (index >= 133 && index <= 142):
          if (!this.board.tiles[index - 11].isFound){this.checkIfBlank(this.board.tiles[index - 11], (index - 11));}
          if (!this.board.tiles[index + 1].isFound){this.checkIfBlank(this.board.tiles[index + 1], (index + 1));}
      case (index == 143):
          if (!this.board.tiles[index - 13].isFound){this.checkIfBlank(this.board.tiles[index - 13], (index - 13));}
          if (!this.board.tiles[index - 12].isFound){this.checkIfBlank(this.board.tiles[index - 12], (index - 12));}
          if (!this.board.tiles[index - 1].isFound){this.checkIfBlank(this.board.tiles[index - 1], (index - 1));}
          break;
    }
  }

  toggleState(event) {
    console.log("hola");
  }

  newGame() {
    this.isPaused = false;
    this.bombs = 20;
    this.board = this.setupBoard();
    this.assignBombs();
    this.generateNumbers();
    this.removeZeros();
    this.isDefusing = false;
    this.bombsLeft = this.bombs;
    this.bombsFound = 0;
    this.time = 0;
    this.tickTimer();
  }
}

