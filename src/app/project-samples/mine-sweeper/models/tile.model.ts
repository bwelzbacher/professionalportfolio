export interface ITile {
    value?: any;
    isFound: boolean;
    isDefused: boolean;
    isExploded: boolean;
  }
  