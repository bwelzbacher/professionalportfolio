import { ITile } from './tile.model';

export interface IBoard {
  tiles: ITile[];
}