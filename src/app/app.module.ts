import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { AlertModule } from 'ngx-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { ScrollEventModule } from 'ngx-scroll-event';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';


import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { AboutMeComponent } from './about-me/about-me.component';
import { SkillsComponent } from './skills/skills.component';
import { ExperienceComponent } from './experience/experience.component';
import { EducationComponent } from './education/education.component';
import { ResumeComponent } from './resume/resume.component';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';
import { ProjectSamplesComponent } from './project-samples/project-samples.component';
import { HomePageComponent } from './home-page/home-page.component';
import { MineSweeperComponent } from './project-samples/mine-sweeper/mine-sweeper.component';
import { NewGameModalComponent } from './project-samples/mine-sweeper/new-game-modal/new-game-modal.component';
import { YahtzeeComponent } from './project-samples/yahtzee/yahtzee.component';
import { MatchesComponent } from './project-samples/matches/matches.component';
import { NewGamesModalComponent } from './project-samples/matches/new-games-modal/new-games-modal.component';
import { ImageModalComponent } from './project-samples/image-modal/image-modal.component';

export const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'samples', component: ProjectSamplesComponent },
  { path: 'resume', component: ResumeComponent },
  { path: 'mine_sweeper', component: MineSweeperComponent},
  {path: 'yahtzee', component: YahtzeeComponent},
  {path: 'matches', component: MatchesComponent}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    AboutMeComponent,
    SkillsComponent,
    ExperienceComponent,
    EducationComponent,
    ResumeComponent,
    ScrollTopComponent,
    ProjectSamplesComponent,
    HomePageComponent,
    MineSweeperComponent,
    NewGameModalComponent,
    YahtzeeComponent,
    MatchesComponent,
    NewGamesModalComponent,
    ImageModalComponent
  ],
  imports: [
  BootstrapModalModule,
  RouterModule.forRoot(
      routes,
      { enableTracing: true }
    ),
    AlertModule.forRoot(),
    BrowserModule,
    ScrollEventModule,
    routing
  ],
  providers: [],
  entryComponents: [
     NewGameModalComponent,
     NewGamesModalComponent,
     ImageModalComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
